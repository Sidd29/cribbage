## Introduction
Our project uses Spring Boot server to handle backend framework, game logic, data interaction and create the server-side logic and endpoints. Specific endpoints have been created using Spring Boot and using GET and POST mappings to handle different types of requests from the frontend. The frontend communicates with the backend by polling and making HTTP requests (GET, POST, etc.) like:
GET Requests: Used to retrieve data from the server. For example, fetching user details, data to display on the page, etc.
POST Requests: Used to send data to the server. For example, sending user inputs, form submissions, etc.
Frontend and backend interaction is done using npm live-server. We have used it to serve the frontend files locally during development. It helped to visualize and interact with the frontend interface. Frontend code consists of HTML, CSS, JavaScript which is responsible for user interface and interaction. This architecture creates a smooth interaction between the frontend and backend, ensuring data flow and user interaction within our game.

## Steps to run game server and start game:

```
Run DemoApplication.java in intellij
Open terminal
Open directory “Frontend” in the game folder
Run: npm install -g live-server
Run: live-server
```